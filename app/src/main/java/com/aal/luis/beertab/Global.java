package com.aal.luis.beertab;

import android.app.Application;
import android.support.v4.widget.DrawerLayout;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;

/**
 * Created by Luis Valenzuela on 4/17/2016.
 */

/* This function is meant to store any global variables that we may need. If you want to add a variable
*  just declare it here and make getter and setter methods*/
public class Global {
    private static Global instance;

    private DrinkArray drinkOrder = new DrinkArray();

    ArrayList<Orders> orderList = new ArrayList<Orders>();


    private int orderNumber;
    private int orderId;
    int ordersLength =0;
    private int progressStatus;;
    private String orderType;
    private String[] navTitles = {"Home", "Review Order", "Order Status", "Order History"};
    private String[] menuTitles = {"Home", "All Drinks", "Alcoholic Drinks", "Non-Alcoholic", "Order History"};
   // private String[] drawerTitles = {"Home", "Menu", "Cart"};

    private DrawerLayout drawerLayout;
    private ListView drawerContents;



    private Global(){}

    public static synchronized Global getInstance() {
        if (instance == null) {
            instance = new Global();
        }
        return instance;
   }

    public String[] getNavTitles() {
        return navTitles;
    }

    public String[] getMenuTitles() { return menuTitles;}



    public DrinkArray getOrderContents() {
        return drinkOrder;
    }

    public void addDrinkToOrder(Drink drink) {
        this.drinkOrder.add(drink);
    }

    public void setEntireOrder(DrinkArray drinkArray) {
        drinkOrder = drinkArray;
    }

    public void setOrderNumber (int number)
    {
        orderNumber = number;
    }
    public void addOrder (Orders order)
    {

        orderList.add(order);
        ordersLength++;

        
    }

    public int gerOrdersLength()
    {
        return ordersLength;
    }

    public int getOrderNumber()
    {
        return orderNumber;
    }

    public int gerOrderID()
    {
        orderId = ordersLength;
        return orderId;
    }

    public void setOrderType(String type)
    {
        orderType = type;
    }

    public String getOrderType()
    {
        return orderType;
    }

    public Orders getOrderkAtIndex(int index) {
        return orderList.get(index);
    }

    public int getProgressStatus()
    {
        return progressStatus;
    }

    public void setProgressStatus(int status)
    {
        progressStatus = status;
    }


}
