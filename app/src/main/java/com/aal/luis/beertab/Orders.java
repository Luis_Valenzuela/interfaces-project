package com.aal.luis.beertab;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;

/**
 * Created by maham on 05/05/16.
 */
public class Orders {

    private DrinkArray orders = new DrinkArray();

    private Timestamp orderDate;
    private String orderStatus;
    private int orderId;
    private int orderNumber;
    private double orderTotal;
    private DrinkArray currentOrder;


    public Orders(DrinkArray order, int orderID, Timestamp orderdate,int orderNum, double orderTot) {

        orders = order;
        orderDate =orderdate;
        orderId =orderID;
        orderNumber = orderNum;
        orderTotal = orderTot;
        currentOrder = order;
    }


    public DrinkArray getOrders() {
        return orders;
    }

    public double getOrderTotal()
    {
        return orderTotal;
    }

    public int getOrderId()
    {
        return orderId;
    }


    public Timestamp getOrderDate()
    {
        return orderDate;
    }

    public void setOrderStatus(String status)
    {
        orderStatus = status;
    }

    public String getOrderStatus()
    {
        return orderStatus;
    }

    public int getOrderNumber()
    {
        return orderNumber;
    }

    public DrinkArray getOrderContents()
    {
        return currentOrder;
    }
}
