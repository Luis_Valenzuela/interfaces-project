package com.aal.luis.beertab;

import android.app.Activity;
import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.RelativeLayout;
import android.view.ViewGroup.LayoutParams;

import java.sql.Timestamp;
import java.util.Calendar;

public class ReviewOrderActivity extends BaseActivity {
    Global global = Global.getInstance();
    public Button updateOrder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        updateOrder = (Button) findViewById(R.id.updateOrder);
        setContentView(R.layout.activity_review_order);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbarOrder);
        toolbar.setTitle("Your Drinks");

        final ListView listView = (ListView) findViewById(R.id.drinkOrders);

        System.out.println("Global order: " + global.getOrderContents().getDrinkArrayLength());

        listView.setAdapter(new DrinkOrderAdapter(this, global.getOrderContents()));

        calculateOrderTotal();
        setDrawerContents(0);

        FloatingActionButton reviewOrderButton = (FloatingActionButton) findViewById(R.id.orderProgressButton);
        reviewOrderButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar calendar = Calendar.getInstance();
                java.util.Date now = calendar.getTime();
                Timestamp orderDate = new java.sql.Timestamp(now.getTime());
                final Orders order = new Orders(global.getOrderContents(), global.gerOrderID(), orderDate,global.getOrderNumber(),global.getOrderContents().getOrderTotal());
                global.addOrder(order);



                Thread thread = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        HTTPPost http = new HTTPPost();

                        System.out.println("\n\n\n Button Clicked \n\n\n");
                        try {
                            http.sendOrder(order);
                        }
                        catch(Exception e) {
                            System.out.println("\n\n\n"+ e.toString());
                        }
                    }
                });
                thread.start();

                Intent intent = new Intent(view.getContext(), OrderProgress.class);
                startActivity(intent);
            }
        });

    }


    

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_review_order, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void calculateOrderTotal () {
        TextView orderTotal = (TextView) findViewById(R.id.reviewOrderTotal);
        orderTotal.setText(String.format( "Total: $%.2f", global.getOrderContents().getOrderTotal()));
    }

   /* public void updateOrder (View v) {
        v.findViewById(R.id.updateOrder).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {

                if (updateOrder.getText().toString().equals("UPDATE")) {
                    updateOrder.setEnabled(true);
                    updateOrder.setText("Save");
                } else if (updateOrder.getText().toString().equals("Save")) {
                    DrinkArray currentOrder = global.getOrderContents();
                    currentOrder.getDrinkAtIndex(position).setDrinkQty(viewHolder.drinkQuantitySpinner.getSelectedItemPosition());
                    Toast.makeText(vv.getContext(), "Quantity updated", Toast.LENGTH_SHORT).show();
                    global.setEntireOrder(currentOrder);
                    viewHolder.drinkQuantitySpinner.setEnabled(false);
                    viewHolder.drinkUpdateOrderButton.setText("UPDATE");
                }


            }
        });
    }*/
}
