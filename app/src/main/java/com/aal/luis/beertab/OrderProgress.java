package com.aal.luis.beertab;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.app.Activity;
import android.os.Handler;
import android.view.Menu;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.app.Dialog;
import android.widget.Button;
import android.widget.Toast;
import java.text.DecimalFormat;


public class OrderProgress extends BaseActivity {

    private ProgressBar progressBar;

    private TextView orderType;
    private TextView orderInfo;
    private Handler handler = new Handler();
    Global global = Global.getInstance();
    Button tip15 ;
    Button tip18 ;
    Button tip20 ;
    Button noTip ;
    Button chargeTip;
    TextView totalAmount;
    public double tipAmnout;
    ImageButton refresh;
    ImageView imageView;
    public View vv = null;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_progress);


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Progress");
        //setSupportActionBar(toolbar);

        imageView = (ImageView) findViewById(R.id.progress_bar);

        refresh = (ImageButton) findViewById(R.id.refresh);
        imageView.setImageResource(R.drawable.received);
        for (int i =0; i<global.gerOrdersLength(); i++)
        {
            if (global.getOrderkAtIndex(i).getOrderId()== global.gerOrderID()-1)
            {
                global.getOrderkAtIndex(i).setOrderStatus(String.valueOf(global.getProgressStatus()));
                //System.out.println("found "+ global.getOrderkAtIndex(i).getOrderStatus());
            }
        }

        refresh.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        vv = v;
                        Thread thread = new Thread(new Runnable() {
                            @Override
                            public void run() {
                                HTTPPost post = new HTTPPost();
                                try {
                                    if (post.getOrderProgress(global.gerOrderID()-1,global.getOrderNumber()).equals( "0")) {
                                        global.setProgressStatus( 0);
                                    } else if (post.getOrderProgress(global.gerOrderID() -1 ,global.getOrderNumber()).equals( "1")) {
                                        global.setProgressStatus( 1);
                                    } else if (post.getOrderProgress(global.gerOrderID()-1,global.getOrderNumber()).equals( "2")) {
                                        global.setProgressStatus( 2);
                                    }

                                } catch (Exception e) {
                                    System.out.println(e);
                                }
                            }
                        });
                        thread.start();
                        if (global.getProgressStatus() == 0) {
                            imageView.setImageResource(R.drawable.received);
                        } else if (global.getProgressStatus() == 1) {
                            imageView.setImageResource(R.drawable.progress);
                        } else if (global.getProgressStatus() == 2) {
                            imageView.setImageResource(R.drawable.complete);
                            tip();
                        }
                        Toast.makeText(vv.getContext(),"Refreshed", Toast.LENGTH_SHORT).show();
                        for (int i =0; i<global.gerOrdersLength(); i++)
                        {
                            if (global.getOrderkAtIndex(i).getOrderId()== global.gerOrderID()-1)
                            {
                                global.getOrderkAtIndex(i).setOrderStatus(String.valueOf(global.getProgressStatus()));
                                //System.out.println("found "+ global.getOrderkAtIndex(i).getOrderStatus());
                            }
                        }
                    }
                });



                        orderType = (TextView) findViewById(R.id.orderType);
                        orderInfo = (TextView) findViewById(R.id.orderInfo);

                        if (global.getOrderType().equals("bar")) {
                            orderType.setText("Your Order Number Is:");
                            int temp = global.getOrderNumber();
                            orderInfo.setText(Integer.toString(temp));


                        } else if (global.getOrderType().equals("table")) {
                            orderType.setText("Your order will be brought to your table by your waiter");

                        }


                        setDrawerContents(0);

/*        progressBar = (ProgressBar) findViewById(R.id.progressBar1);
        textView = (TextView) findViewById(R.id.textView1);
        // Start long running operation in a background thread
        new Thread(new Runnable() {
            public void run() {
                while (progressStatus < 100) {
                    progressStatus += 1;
                    // Update the progress bar and display the
                    //current value in the text view
                    handler.post(new Runnable() {
                        public void run() {
                            progressBar.setProgress(progressStatus);
                            textView.setText(progressStatus+"/"+progressBar.getMax());
                        }
                    });
                    try {
                        // Sleep for 200 milliseconds.
                        //Just to display the progress slowly
                        Thread.sleep(200);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }).start();*/


    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items
        //to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_review_order, menu);
        return true;
    }

    public void tip() {
        final Dialog tippingDialog = new Dialog(this);
        tippingDialog.setContentView(R.layout.tip_alert);
        tippingDialog.setTitle("Thank you for your order");


        tip15 = (Button)tippingDialog.findViewById(R.id.tip15);
        tip18 = (Button) tippingDialog.findViewById(R.id.tip18);
        tip20 = (Button) tippingDialog.findViewById(R.id.tip20);
        noTip =(Button) tippingDialog.findViewById(R.id.noTip);
        chargeTip = (Button) tippingDialog.findViewById(R.id.chargeTip);

        tipAmnout = global.getOrderContents().getOrderTotal();

        tip15.setText("15% \n "+"$"+ new DecimalFormat("##.##").format(tipAmnout * 0.15));
        tip18.setText("18% \n "+"$"+ new DecimalFormat("##.##").format(tipAmnout * 0.18));
        tip20.setText("20% \n "+ "$"+ new DecimalFormat("##.##").format(tipAmnout * 0.2));
        totalAmount = (TextView) tippingDialog.findViewById(R.id.totalAmount);
        totalAmount.setText("$"+ global.getOrderContents().getOrderTotal());

        tip15.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        tipAmnout = global.getOrderContents().getOrderTotal()*0.15;
                        tipAmnout +=  global.getOrderContents().getOrderTotal();
                        totalAmount.setText("$"+ new DecimalFormat("##.##").format(tipAmnout));
                        tip15.setEnabled(false);
                        tip18.setEnabled(true);
                        tip20.setEnabled(true);
                    }
                }
        );

        tip18.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        tipAmnout = global.getOrderContents().getOrderTotal()*0.18;
                        tipAmnout +=  global.getOrderContents().getOrderTotal();
                        totalAmount.setText("$"+ new DecimalFormat("##.##").format(tipAmnout));
                        tip18.setEnabled(false);
                        tip15.setEnabled(true);
                        tip20.setEnabled(true);
                    }
                }
        );

        tip20.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        tipAmnout = global.getOrderContents().getOrderTotal()*0.2;
                        tipAmnout +=  global.getOrderContents().getOrderTotal();
                        totalAmount.setText("$"+ new DecimalFormat("##.##").format(tipAmnout));
                        tip20.setEnabled(false);
                        tip18.setEnabled(true);
                        tip15.setEnabled(true);
                    }
                }
        );

        noTip.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        tippingDialog.dismiss();
                    }
                }
        );

        chargeTip.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Toast.makeText(v.getContext(), "Tip successfully submitted!", Toast.LENGTH_SHORT).show();
                        tippingDialog.dismiss();

                        DrinkArray nullarr = new DrinkArray();
                        global.setEntireOrder(nullarr);
                    }
                }
        );

        tippingDialog.show();



    }


}