package com.aal.luis.beertab;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.ListView;

/**
 * Created by maham on 05/05/16.
 */
public class OrderDetailsActivity extends AppCompatActivity {

    Global global = Global.getInstance();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history_details);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbarHistory);
        toolbar.setTitle("Order Details");

        final ListView listView = (ListView) findViewById(R.id.historyDetails);
        Intent intent = getIntent();
        Bundle extras = intent.getExtras();
        String id = extras.getString("EXTRA_ORDER_ID");
        for (int i =0; i< global.gerOrdersLength(); i++) {
            if (global.getOrderkAtIndex(i).getOrderId() == Integer.parseInt(id)) {
                listView.setAdapter(new HistoryDetailsAdapter(this, global.getOrderkAtIndex(i).getOrderContents()));

            }
        }
    }

}
