package com.aal.luis.beertab;

import android.app.Application;
import android.content.Context;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.content.Intent;
import android.widget.Toast;


import java.util.ArrayList;

/**
 * Created by Luis Valenzuela on 3/20/2016.
 */

/*
* This class extends BaseAdapter and serves as a way to insert multiple items into a single row of
* a list view.
* */
public class DrinkBaseAdapter extends BaseAdapter {
    Global global = Global.getInstance();
    private static DrinkArray drinkArrayList;
    private static DrinkArray drinkOrder = new DrinkArray();
    private LayoutInflater inflater;

    private Context activityContext;

    public View vv = null;


    public DrinkBaseAdapter(Context context, DrinkArray drinkList) {
        drinkArrayList = drinkList;
        inflater = LayoutInflater.from(context);
        activityContext = context;
    }

    public int getCount() {
        return drinkArrayList.getDrinkArrayLength();
    }

    public Drink getItem(int index) {
        return drinkArrayList.getDrinkAtIndex(index);
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(final int position, View convertView, final ViewGroup parent) {
        final MultiViewHolder viewHolder;
        //RecyclerView.ViewHolder holder;
        //View tempView = new View
        vv = convertView;

        if (convertView == null) {

            convertView = inflater.inflate(R.layout.multi_row_view, null);
            viewHolder = new MultiViewHolder();
            viewHolder.drinkImageView = (ImageView) convertView.findViewById(R.id.drinkImage);
            viewHolder.drinkNameView = (TextView) convertView.findViewById(R.id.drinkName);
            viewHolder.drinkCostView = (TextView) convertView.findViewById(R.id.drinkCost);
            viewHolder.addDrinkButton = (Button) convertView.findViewById(R.id.addDrinkButton);
            viewHolder.drinkQty = (Spinner) convertView.findViewById(R.id.drinkQty);
            viewHolder.drinkDescription = (TextView) convertView.findViewById(R.id.drinkDescription);


            convertView.setTag(viewHolder);
        } else {
            viewHolder = (MultiViewHolder) convertView.getTag();

        }
        int pic = drinkArrayList.getDrinkAtIndex(position).getDrinkImageId();
        viewHolder.drinkImageView.setImageResource(pic);
        viewHolder.drinkNameView.setText(drinkArrayList.getDrinkAtIndex(position).getDrinkName());
        viewHolder.drinkDescription.setText(drinkArrayList.getDrinkAtIndex(position).getDrinkDescription());
        viewHolder.drinkCostView.setText("$"+Double.toString(drinkArrayList.getDrinkAtIndex(position).getDrinkCost()));
        viewHolder.addDrinkButton.setVisibility(viewHolder.addDrinkButton.VISIBLE);
        viewHolder.drinkQty.setVisibility(viewHolder.drinkQty.VISIBLE);

        final Button addOrder = (Button) convertView.findViewById(R.id.addDrinkButton);
        addOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*addDrink.addDrink(0, Integer.parseInt(viewHolder.drinkQty.getSelectedItem().toString()));
                drinkBag.addDrink(0,viewHolder.drinkNameView.getText().toString(),0,null,Integer.parseInt(viewHolder.drinkQty.getSelectedItem().toString()));*//*
              Drink currentDrink = drinkArrayList.getDrinkAtIndex(position);
                drinkOrder.addDrink(currentDrink.getDrinkID(), currentDrink.getDrinkName(), currentDrink.getDrinkCost(), currentDrink.getDrinkImageId(), viewHolder.drinkQty.getSelectedItemPosition());
                global.setEntireOrder(drinkOrder);*/

                if (viewHolder.drinkQty.getSelectedItemPosition() == 0)
                {
                    Toast.makeText(vv.getContext(), "please add a quantity", Toast.LENGTH_SHORT).show();
                }
                else {
                    Drink currentDrink = drinkArrayList.getDrinkAtIndex(position);
                    drinkOrder.addDrink(currentDrink.getDrinkID(), currentDrink.getDrinkName(), currentDrink.getDrinkCost(), null, null, currentDrink.getDrinkImageId(), viewHolder.drinkQty.getSelectedItemPosition());
                    global.setEntireOrder(drinkOrder);
                    Toast.makeText(activityContext, "Drink added", Toast.LENGTH_SHORT).show();
                }
            }
        });
        //setButtonListener(viewHolder, position);
        return convertView;
    }

/*    private void setButtonListener(final MultiViewHolder viewHolder, final int position) {
        viewHolder.addDrinkButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {

                if (viewHolder.drinkQty.getSelectedItemPosition() == 0) {
                    Toast.makeText(activityContext, "Please Select Quantity", Toast.LENGTH_SHORT).show();
                }
                else {
                    Drink drinkToAdd = drinkArrayList.getDrinkAtIndex(position);
                    global.getOrderContents().add(drinkToAdd);

                    Toast.makeText(activityContext, "Drink Added", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }*/

    /*
    * This class contains all of the views that will be put into the list view. These should match
    * the fields in the layouts that will be used.
    * */
    static class MultiViewHolder {
        ImageView drinkImageView;
        TextView drinkNameView;
        TextView drinkCostView;
        TextView drinkDescription;
        Button addDrinkButton;
        Spinner drinkQty;
    }

}
