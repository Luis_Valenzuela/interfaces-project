package com.aal.luis.beertab;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class SignupActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void createUser(View view){
        DatabaseHelper db = new DatabaseHelper(getApplicationContext());
        EditText username = (EditText) findViewById(R.id.input_name);
        EditText password = (EditText) findViewById(R.id.input_password);
        String result = db.newUser(username.getText().toString(), password.getText().toString());
        Toast.makeText(getApplicationContext(), result, Toast.LENGTH_SHORT).show();
        if(result == "User created!"){
            Intent intent = new Intent(this, LoginActivity.class);
            startActivity(intent);
            finish();
        }
    }

}
