package com.aal.luis.beertab;

import android.content.Intent;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

public class BaseActivity extends AppCompatActivity {
    private Global global;
    protected DrawerLayout fullLayout;
    protected FrameLayout frameLayout;
    protected ImageView header;
    ListView drawerContents;
    private View view;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        global = Global.getInstance();
        super.onCreate(savedInstanceState);
    }

    @Override
    public void setContentView(int layoutResID) {
        fullLayout = (DrawerLayout) getLayoutInflater().inflate(R.layout.activity_base, null);
        frameLayout = (FrameLayout) fullLayout.findViewById(R.id.content_frame);


        getLayoutInflater().inflate(layoutResID, frameLayout, true);
        super.setContentView(fullLayout);
    }

    public void setDrawerContents(int drawerType) {
        drawerContents = (ListView) findViewById(R.id.drawerListView);
        if (drawerType == 0) {
            drawerContents.setAdapter(new ArrayAdapter<String>(this, R.layout.drawer_list_item, global.getNavTitles()));
        }
        else if (drawerType == 1) {
            drawerContents.setAdapter(new ArrayAdapter<String>(this, R.layout.drawer_list_item, global.getMenuTitles()));
        }

        drawerContents.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                System.out.println("Tapped: " + drawerContents.getItemAtPosition(position).toString());
            }
        });
        setListener(drawerContents);

       // System.out.println("Tepped: " + drawerContents.getSelectedItem().toString());
        //header = (ImageView) view.findViewById(R.id.headerImage);

        //System.out.println("Header: " + header);

        //Toast.makeText(view.getContext(), "Header: " + header, Toast.LENGTH_SHORT).show();
        //header.setImageResource(R.drawable.header);
    }

    public void setListener(final ListView drawerContents) {
        drawerContents.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Object selectedItem = drawerContents.getItemAtPosition(position);
                String itemString = selectedItem.toString();
                Intent intent;

                if (itemString.equals("Home")) {
                    intent = new Intent(view.getContext(), MainActivity.class);
                    startActivity(intent);
                }
                else if (itemString.equals("Alcoholic Drinks")) {
                    intent  = new Intent(view.getContext(), DrinkMenuActivity.class);
                    Bundle extras = new Bundle();

                    extras.putString("EXTRA_DRINK_TYPE", "alc");

                    intent.putExtras(extras);
                    startActivity(intent);
                }
                else if (itemString.equals("Non-Alcoholic")) {
                    intent  = new Intent(view.getContext(), DrinkMenuActivity.class);
                    Bundle extras = new Bundle();

                    extras.putString("EXTRA_DRINK_TYPE", "soft");

                    intent.putExtras(extras);
                    startActivity(intent);
                }
                else if (itemString.equals("Review Order")) {
                    intent = new Intent(view.getContext(), ReviewOrderActivity.class);
                    startActivity(intent);
                }
                else if (itemString.equals("All Drinks")) {
                    intent = new Intent(view.getContext(), DrinkMenuActivity.class);
                    Bundle extras = new Bundle();

                    extras.putString("EXTRA_DRINK_TYPE", "all");

                    intent.putExtras(extras);
                    startActivity(intent);
                }
                else if (itemString.equals("Order History")) {
                    intent = new Intent(view.getContext(), HistoryActivity.class);
                    startActivity(intent);
                }

                //.out.println("Tapped: " + drawerContents.getItemAtPosition(position).toString());
            }
        });
    }
}
