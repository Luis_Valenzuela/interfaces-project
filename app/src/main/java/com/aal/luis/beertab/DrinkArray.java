package com.aal.luis.beertab;

import java.util.ArrayList;

/**
 * Created by Luis Valenzuela on 3/20/2016.
 */

/*
* This class stores drinks in an array list. It will store the total cost for a given drink order.
* Removing or adding a drink will automatically adjust the total.
* */
public class DrinkArray {

    private ArrayList<Drink> drinkArrayList;
    private int length = 0;
    private double orderTotal = 0;

    public DrinkArray() {
        drinkArrayList = new ArrayList<Drink>();
    }

    public void addDrink(int drinkID, String drinkName, double drinkCost,String description,String category, int drinkImageId,int drinkQty) {
        Drink drink = new Drink();

        drink.setDrinkID(drinkID);
        drink.setDrinkName(drinkName);
        drink.setDrinkCost(drinkCost);
        drink.setDrinkdescription(description);
        drink.setDrinkCat(category);
        drink.setDrinkQty(drinkQty);
        drink.setDrinkImageId(drinkImageId);

        drinkArrayList.add(drink);
        orderTotal += drinkCost * drinkQty;
        length++;
    }

    public void add(Drink drink) {
        drinkArrayList.add(drink);
        orderTotal += drink.getDrinkCost() * drink.getDrinkQty();
        length++;
    }

    public Drink getDrinkAtIndex(int index) {
        return drinkArrayList.get(index);
    }

    public ArrayList getDrinkArray (){
        return drinkArrayList;
    }

    public int getDrinkArrayLength() {
        return length;
    }

    public void removeDrink(int index)
    {
        orderTotal -= drinkArrayList.get(index).getDrinkCost() * drinkArrayList.get(index).getDrinkQty();
        drinkArrayList.remove(index);
        length--;
    }

   public double getOrderTotal() {
        return this.orderTotal;
    }

    public void updateDrinkQtyAtIndex(int index, int quantity) {
        orderTotal -= drinkArrayList.get(index).getDrinkCost() * drinkArrayList.get(index).getDrinkQty();

        drinkArrayList.get(index).setDrinkQty(quantity);

        orderTotal += drinkArrayList.get(index).getDrinkCost() * drinkArrayList.get(index).getDrinkQty();
    }

}
