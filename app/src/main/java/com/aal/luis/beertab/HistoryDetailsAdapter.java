package com.aal.luis.beertab;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by maham on 05/05/16.
 */
public class HistoryDetailsAdapter extends BaseAdapter {

    private static DrinkArray order;
    private LayoutInflater inflater;
    public View vv = null;

    public HistoryDetailsAdapter(Context context, DrinkArray drinkList) {
        order = drinkList;
        inflater = LayoutInflater.from(context);
    }

    public int getCount() {
        return order.getDrinkArrayLength();
    }

    public Drink getItem(int index) {
        return order.getDrinkAtIndex(index);
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {
        final MultiViewHolder viewHolder;
        vv = convertView;

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.history_details, null);
            viewHolder = new MultiViewHolder();

            viewHolder.drinkImageView = (ImageView) convertView.findViewById(R.id.drinkHistoryImage);
            viewHolder.drinkTextView = (TextView) convertView.findViewById(R.id.drinkHistoryQuantityText);
            viewHolder.drinkQuantitySpinner = (Spinner) convertView.findViewById(R.id.drinkHistorySpinner);
            viewHolder.drinkNameView = (TextView) convertView.findViewById(R.id.drinkNameHistory);
            viewHolder.drinkCostView = (TextView) convertView.findViewById(R.id.drinkCostHistory);
            convertView.setTag(viewHolder);
            viewHolder.name = (TextView) convertView.findViewById(R.id.textView4History);
            viewHolder.price = (TextView) convertView.findViewById(R.id.textView5History);
            viewHolder.qty = (TextView) convertView.findViewById(R.id.drinkHistoryQuantityText);


        } else {
            viewHolder = (MultiViewHolder) convertView.getTag();
        }

        viewHolder.drinkImageView.setImageResource(order.getDrinkAtIndex(position).getDrinkImageId());
        viewHolder.drinkQuantitySpinner.setSelection(order.getDrinkAtIndex(position).getDrinkQty());
        viewHolder.drinkQuantitySpinner.setEnabled(false);
        viewHolder.drinkQuantitySpinner.setVisibility(View.VISIBLE);
        viewHolder.drinkNameView.setText(order.getDrinkAtIndex(position).getDrinkName());
        viewHolder.drinkCostView.setText("$" + String.format(Double.toString(order.getDrinkAtIndex(position).getDrinkCost() *
                order.getDrinkAtIndex(position).getDrinkQty())));
        viewHolder.qty.setVisibility(View.VISIBLE);
        viewHolder.name.setVisibility(View.VISIBLE);
        viewHolder.price.setVisibility(View.VISIBLE);

        return convertView;
    }



    static class MultiViewHolder {
        ImageView drinkImageView;
        TextView drinkTextView;
        Spinner drinkQuantitySpinner;
        TextView drinkNameView;
        TextView drinkCostView;
        TextView name;
        TextView price;
        TextView qty;
    }
}
