package com.aal.luis.beertab;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.Toast;

public class DrinkMenuActivity extends BaseActivity {
    Global global = Global.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drink_menu);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Drink Menu");

        System.out.println("getSrinks");
        DrinkArray drinksToShow = getDrinks();


        final ListView listView = (ListView) findViewById(R.id.drinkMenu);
        listView.setAdapter(new DrinkBaseAdapter(this, drinksToShow));

        setDrawerContents(1);


        FloatingActionButton reviewOrderButton = (FloatingActionButton) findViewById(R.id.reviewOrderButton);


        reviewOrderButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (global.getOrderContents().getDrinkArrayLength() == 0)
                    {
                        Toast.makeText(getApplicationContext(), "No drinks have been added yet!", Toast.LENGTH_SHORT).show();
                    }
                    else {
                        Intent intent = new Intent(view.getContext(), ReviewOrderActivity.class);
                        startActivity(intent);
                    }
                }
            });



    }

    public DrinkArray getDrinks() {
        Intent intent = getIntent();
        Bundle extras = intent.getExtras();
        DrinkArray allDrinks = initDatabase();
        //DrinkArray drinksToShow = allDrinks;
        //String personName = extras.getString("EXTRA_PERSON_NAME");
        //String tableNumber = extras.getString("EXTRA_TABLE_NUMBER");
        String drinkType = extras.getString("EXTRA_DRINK_TYPE");

        if (drinkType != null) {
            if (drinkType.equals("soft")) {
                return getNonAlcohol(allDrinks);
            } else if (drinkType.equals("alc")) {
                return getAlcohol(allDrinks);
            }
            else if (drinkType.equals("all")) {
                return allDrinks;
            }
        }
        System.out.println("TYPE: " + drinkType);
        //Toast.makeText(this, drinkType, Toast.LENGTH_LONG).show();
        return allDrinks;


    }

    public DrinkArray getNonAlcohol(DrinkArray drinkArray) {
        DrinkArray nonAlcArray = new DrinkArray();
        for (int i = 0; i < drinkArray.getDrinkArrayLength(); i++) {
            if (drinkArray.getDrinkAtIndex(i).getDrinkCat().equals("soft")) {
                nonAlcArray.add(drinkArray.getDrinkAtIndex(i));
            }
        }

        return nonAlcArray;
    }

    public DrinkArray getAlcohol(DrinkArray drinkArray) {
        DrinkArray alcArray = new DrinkArray();
        for (int i = 0; i < drinkArray.getDrinkArrayLength(); i++) {
            if (drinkArray.getDrinkAtIndex(i).getDrinkCat().equals("cocktail") || drinkArray.getDrinkAtIndex(i).getDrinkCat().equals("beer")) {
                alcArray.add(drinkArray.getDrinkAtIndex(i));
            }
        }
        return alcArray;
    }

    public DrinkArray initDatabase() {
        DatabaseHelper dbHelper = new DatabaseHelper(this);
        DrinkArray drinkArray= new DrinkArray();

        //drinkArray = dbHelper.getDrinkCat("beer");
        //System.out.println("name is "+ drinkArray.getDrinkArrayLength());



        drinkArray.addDrink(1,"heineken", 3, "heineken 12 OZ bottle", "beer", R.mipmap.ic_heineken,0);
        drinkArray.addDrink(2,"budweiser", 4, "budweiser 12 OZ bottle", "beer", R.mipmap.ic_budweiser,0);
        drinkArray.addDrink(3,"corona", 4, "corona 12 OZ bottle", "beer", R.mipmap.ic_corona,0);


        drinkArray.addDrink(4,"martini", 6, "Vodka Martini", "cocktail", R.mipmap.ic_martini,0);
        drinkArray.addDrink(5,"mojito", 10, "low carb mojito", "cocktail", R.mipmap.ic_mojito,0);
        drinkArray.addDrink(6,"old fashioned", 7, "whiskey with a twist of citrus rind", "cocktail", R.mipmap.ic_oldfashion,0);

        drinkArray.addDrink(7,"pepsi", 1, "12 OZ can", "soft", R.mipmap.peppep,0);
        drinkArray.addDrink(8,"Coca-Cola", 1, "12 OZ can", "soft", R.mipmap.ic_coke,0);
        drinkArray.addDrink(9,"dr. pepper",1,"12 OZ can","soft",R.mipmap.ic_pepper,0);
        /*************************************************************
         int i = 0;
         while (i < 10) {
         drinkArray.addDrink(i, "Test Drink: " + i, 2.00, 0,0);
         //global.addDrinkToOrder(drinkArray.getDrinkAtIndex(i));
         //System.out.println("Global: " + global.getOrderContents().getDrinkAtIndex(i).getDrinkName());
         i++;
         }*/

        return drinkArray;
    }


}
