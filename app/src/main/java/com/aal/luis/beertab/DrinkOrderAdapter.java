package com.aal.luis.beertab;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;


/**
 * Created by Luis Valenzuela on 4/19/2016.
 */
public class DrinkOrderAdapter extends BaseAdapter {
    Global global = Global.getInstance();
    private static DrinkArray order;
    private LayoutInflater inflater;
    private Context activityContext;
    public View vv = null;

    public DrinkOrderAdapter(Context context, DrinkArray drinkList) {
        order = drinkList;
        inflater = LayoutInflater.from(context);
        this.activityContext = context;
    }

    public int getCount() { return order.getDrinkArrayLength();}

    public Drink getItem(int index) { return order.getDrinkAtIndex(index);}

    public long getItemId(int position) { return position; }

    public View getView (final int position, View convertView, ViewGroup parent) {
        final MultiViewHolder viewHolder;
        vv = convertView;

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.multi_row_review_order, null);
            viewHolder = new MultiViewHolder();

            viewHolder.drinkImageView = (ImageView) convertView.findViewById(R.id.drinkOrderImage);
            viewHolder.drinkTextView = (TextView) convertView.findViewById(R.id.drinkOrderQuantityText);
            viewHolder.drinkQuantitySpinner = (Spinner) convertView.findViewById(R.id.drinkOrderSpinner);
            viewHolder.drinkNameView = (TextView) convertView.findViewById(R.id.drinkNameOrder);
            viewHolder.drinkCostView = (TextView) convertView.findViewById(R.id.drinkCostOrder);
            viewHolder.drinkRemoveButton = (Button) convertView.findViewById(R.id.removeOrder);
            viewHolder.drinkUpdateOrderButton = (Button) convertView.findViewById(R.id.updateOrder);

            convertView.setTag(viewHolder);

        }
        else {
            viewHolder = (MultiViewHolder) convertView.getTag();
        }

        viewHolder.drinkImageView.setImageResource(order.getDrinkAtIndex(position).getDrinkImageId());
        viewHolder.drinkQuantitySpinner.setSelection(order.getDrinkAtIndex(position).getDrinkQty());
        viewHolder.drinkQuantitySpinner.setEnabled(false);
        viewHolder.drinkNameView.setText(order.getDrinkAtIndex(position).getDrinkName());
        viewHolder.drinkCostView.setText("$"+String.format(Double.toString(order.getDrinkAtIndex(position).getDrinkCost() *
                order.getDrinkAtIndex(position).getDrinkQty())));

        setButtonListeners(viewHolder, position);
        return convertView;
    }

    private void setButtonListeners(final MultiViewHolder viewHolder, final int position) {
        viewHolder.drinkRemoveButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                final int drinkPositon =  position;
                System.out.println("Removing drink at index: " + position);
                //DrinkArray currentOrder = global.getOrderContents();
                global.getOrderContents().removeDrink(position);
                //currentOrder.removeDrink(position);
                Toast.makeText(vv.getContext(), "Drink removed", Toast.LENGTH_SHORT).show();
                //global.setEntireOrder(currentOrder);

                Intent intent = new Intent(activityContext, ReviewOrderActivity.class);
                ((Activity) activityContext).finish();
                activityContext.startActivity(intent);
            }
        });

        viewHolder.drinkUpdateOrderButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {

                if (viewHolder.drinkUpdateOrderButton.getText().toString().equals("Update")) {
                    viewHolder.drinkQuantitySpinner.setEnabled(true);
                    viewHolder.drinkUpdateOrderButton.setText("Save");

                }
                else if (viewHolder.drinkUpdateOrderButton.getText().toString().equals("Save")) {

                    global.getOrderContents().updateDrinkQtyAtIndex(position, viewHolder.drinkQuantitySpinner.getSelectedItemPosition());

                    /*DrinkArray currentOrder = global.getOrderContents();
                    currentOrder.getDrinkAtIndex(position).setDrinkQty(viewHolder.drinkQuantitySpinner.getSelectedItemPosition());
                    Toast.makeText(vv.getContext(), "Quantity updated", Toast.LENGTH_SHORT).show();
                    global.setEntireOrder(currentOrder);*/
                    viewHolder.drinkQuantitySpinner.setEnabled(false);
                    viewHolder.drinkUpdateOrderButton.setText("Update");
                    Toast.makeText(vv.getContext(), "Quantity updated", Toast.LENGTH_SHORT).show();

                    Intent intent = new Intent(activityContext, ReviewOrderActivity.class);
                    ((Activity) activityContext).finish();
                    activityContext.startActivity(intent);
                }
            }
        });



    }


    static class MultiViewHolder {
        ImageView drinkImageView;
        TextView drinkTextView;
        Spinner drinkQuantitySpinner;
        TextView drinkNameView;
        TextView drinkCostView;
        Button drinkRemoveButton;
        Button drinkUpdateOrderButton;
    }
}
