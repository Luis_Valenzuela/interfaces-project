package com.aal.luis.beertab;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import java.util.concurrent.ThreadLocalRandom;



public class MainActivity extends AppCompatActivity {
    Toolbar toolbar;
    DrawerLayout drawerLayout;
    ActionBarDrawerToggle actionBarDrawerToggle;
    Global global = Global.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        String personName = "";
        final String tableNumber = "";


        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Home");
        //setSupportActionBar(toolbar);



        /*drawerLayout = (DrawerLayout)findViewById(R.id.drawer_layout);
        actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar , R.string.drawer_open,R.string.drawer_close);
        drawerLayout.setDrawerListener(actionBarDrawerToggle);*/




        /*NavigationView navigationView = (NavigationView)findViewById(R.id.navigation_view);
        navigationView.setNavigationItemSelectedListener(this);*/
        initOclickHide();

    }
/*    @Override
    protected void onPostCreate(Bundle savedInstanceState){
        super.onPostCreate(savedInstanceState);
        actionBarDrawerToggle.syncState();
    }

    // ------------------------ Drawer Mapping Between Menu To Activities ----------------------------//
    public boolean onNavigationItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.dash_id:
                // Maping the button to Halo Class
                startActivity(new Intent(this, Helo.class));
                break;
            case R.id.alchol_id_id:
                startActivity(new Intent(this, Helo.class));
                break;

            case R.id.nonalchol_id:
                startActivity(new Intent(this, Helo.class));
                break;

        }
        return true;

    }*/

    private void initBothListener()
    {
        FloatingActionButton nextButton = (FloatingActionButton) findViewById(R.id.mainNextButton);
        nextButton.setOnClickListener(new View.OnClickListener() {


            @Override
            public void onClick(View view) {
                EditText nameEditText = (EditText) findViewById(R.id.mainName);
                EditText tableNumberEditText = (EditText) findViewById(R.id.tableNumber);

                if (checkInput(nameEditText.getText().toString()) && checkInput(tableNumberEditText.getText().toString())) {
                    Intent intent = new Intent(view.getContext(), DrinkMenuActivity.class);
                    Bundle extras = new Bundle();

                    extras.putString("EXTRA_PERSON_NAME", nameEditText.getText().toString());
                    extras.putString("EXTRA_TABLE_NUMBER", tableNumberEditText.getText().toString());

                    intent.putExtras(extras);
                    startActivity(intent);
                } else {
                    Snackbar.make(view, "Enter a value for both fields.", Snackbar.LENGTH_LONG).setAction("Action", null).show();
                }

            }
        });
    }


    private void initOneListener()
    {
        FloatingActionButton nextButton = (FloatingActionButton) findViewById(R.id.mainNextButton);
        nextButton.setOnClickListener(new View.OnClickListener() {


            @Override
            public void onClick(View view) {
                EditText nameEditText = (EditText) findViewById(R.id.mainName);
                EditText tableNumberEditText = (EditText) findViewById(R.id.tableNumber);

                if (checkInput(nameEditText.getText().toString())) {
                    Intent intent = new Intent(view.getContext(), DrinkMenuActivity.class);
                    Bundle extras = new Bundle();

                    extras.putString("EXTRA_PERSON_NAME", nameEditText.getText().toString());


                    intent.putExtras(extras);
                    startActivity(intent);
                }
                else {
                    Snackbar.make(view, "Enter your name.", Snackbar.LENGTH_LONG).setAction("Action", null).show();
                }

            }
        });
    }

    public boolean checkInput(String contents) {
        if (contents.equals("")) {
            return false;
        }
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void initOclickHide() {
        final Button tableSeatting = (Button) findViewById(R.id.btnTableOrder);
        final Button barSetting = (Button) findViewById(R.id.btnBarOrder);
        final Button dashboard = (Button) findViewById(R.id.btnDashboard);
        //final TextView prograss = (TextView) findViewById(R.id.progbtn);

        tableSeatting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText tableNumber = ((EditText) findViewById(R.id.tableNumber));
                EditText orderName = (EditText) findViewById(R.id.mainName);
                FloatingActionButton nextButton = (FloatingActionButton) findViewById(R.id.mainNextButton);
                tableNumber.setVisibility(View.VISIBLE);
                orderName.setVisibility(View.VISIBLE);
                nextButton.setVisibility(View.VISIBLE);
                tableSeatting.setVisibility(View.INVISIBLE);
                barSetting.setVisibility(View.INVISIBLE);
                dashboard.setVisibility(View.VISIBLE);
               // prograss.setVisibility(View.INVISIBLE);
                global.setOrderType("table");
                global.setOrderNumber(-1);
                initBothListener();

            }
        });

        barSetting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText tableNumber = ((EditText) findViewById(R.id.tableNumber));
                EditText orderName = (EditText) findViewById(R.id.mainName);
                FloatingActionButton nextButton = (FloatingActionButton) findViewById(R.id.mainNextButton);
                orderName.setVisibility(View.VISIBLE);
                nextButton.setVisibility(View.VISIBLE);
                tableSeatting.setVisibility(View.INVISIBLE);
                barSetting.setVisibility(View.INVISIBLE);
                dashboard.setVisibility(View.VISIBLE);
               // prograss.setVisibility(View.INVISIBLE);
                global.setOrderNumber(ThreadLocalRandom.current().nextInt(0, 99 + 1));
                global.setOrderType("bar");





                initOneListener();


            }
        });

        dashboard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText tableNumber = ((EditText) findViewById(R.id.tableNumber));
                EditText orderName = (EditText) findViewById(R.id.mainName);
                FloatingActionButton nextButton = (FloatingActionButton) findViewById(R.id.mainNextButton);
                orderName.setVisibility(View.INVISIBLE);
                tableNumber.setVisibility(View.INVISIBLE);
                dashboard.setVisibility(View.VISIBLE);
                nextButton.setVisibility(View.INVISIBLE);
                tableSeatting.setVisibility(View.VISIBLE);
                barSetting.setVisibility(View.VISIBLE);
                dashboard.setVisibility(View.INVISIBLE);
                //prograss.setVisibility(View.VISIBLE);



                initOneListener();


            }
        });

    }
}
