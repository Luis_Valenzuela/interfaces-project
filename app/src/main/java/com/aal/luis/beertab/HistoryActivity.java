package com.aal.luis.beertab;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.text.style.UnderlineSpan;
import android.text.SpannableString;
/**
 * Created by maham on 04/05/16.
 */
public class HistoryActivity extends Activity {

    Global global = Global.getInstance();
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.order_history);

        TableLayout tbl = (TableLayout) findViewById(R.id.orderHistory);
        // delcare a new row

        for (int i = 0; i < global.gerOrdersLength(); i++) {
            TableRow newRow = new TableRow(this);
            // add views to the row
            final TextView id = new TextView(this);
            TextView date = new TextView(this);
            TextView status = new TextView(this);
            TextView number = new TextView(this);
            id.setTextColor(Color.BLUE);
            date.setTextColor(Color.WHITE);
            status.setTextColor(Color.WHITE);
            number.setTextColor(Color.WHITE);

            id.setClickable(true);

            SpannableString content = new SpannableString("Content");
            content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
            id.setText(content);

            id.setBackgroundResource(R.drawable.border);
            date.setBackgroundResource(R.drawable.border);
            status.setBackgroundResource(R.drawable.border);
            number.setBackgroundResource(R.drawable.border);
            id.setPadding(5, 5, 5, 5);
            date.setPadding(5, 5, 5, 5);
            status.setPadding(5, 5, 5, 5);
            number.setPadding(5, 5, 5, 5);
            id.setText(String.valueOf(global.getOrderkAtIndex(i).getOrderId()));
            date.setText(global.getOrderkAtIndex(i).getOrderDate().toString());
            number.setText(String.valueOf(global.getOrderkAtIndex(i).getOrderNumber()));
            if (global.getOrderkAtIndex(i).getOrderStatus().equals("0")) {
                status.setText("Received");
            } else if (global.getProgressStatus() == 1) {
                status.setText("In Progress");
            } else if (global.getProgressStatus() == 2) {
                status.setText("Completed");
            }
           // status.setText("completed");
            newRow.addView(id);
            newRow.addView(date);
            newRow.addView(status);
            newRow.addView(number);
            ; // you would actually want to set properties on this before adding it
            // add the row to the table layout
            tbl.addView(newRow);
            id.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(v.getContext(), OrderDetailsActivity.class);
                    Bundle extras = new Bundle();

                    extras.putString("EXTRA_ORDER_ID", id.getText().toString());
                    intent.putExtras(extras);
                    startActivity(intent);
                }
            });

        }

        Button home = (Button) findViewById(R.id.btnHome);

        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(v.getContext(), MainActivity.class);
                startActivity(intent);

            }
        });
    }


}
