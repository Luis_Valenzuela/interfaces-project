package com.aal.luis.beertab;

import android.os.SystemClock;
import android.provider.Settings;
import android.text.method.HideReturnsTransformationMethod;

import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import javax.net.ssl.HttpsURLConnection;

/**
 * Created by Luis Valenzuela on 5/4/2016.
 */
public class HTTPPost {
    Global global = Global.getInstance();
    private URL url;
    private HttpURLConnection connection;


    public void HTTPPost() {}


    public void sendOrder(Orders order) throws Exception {
        URL url = new URL("http://134.114.8.88/website/addOrderToDB.php");
        HttpURLConnection connection;
        DrinkArray drinks = order.getOrders();
        Drink currentDrink = null;

        for (int i = 0; i < drinks.getDrinkArrayLength(); i++) {

            currentDrink = drinks.getDrinkAtIndex(i);
            System.out.println("ID: " + order.getOrderId());
            System.out.println("Total " + order.getOrderTotal());
            System.out.println("Number: " + order.getOrderNumber());
            System.out.println("Date: " + order.getOrderDate());
            System.out.println("Name: " + currentDrink.getDrinkName());
            System.out.println("Qty: " + currentDrink.getDrinkQty());

            String postContent = "&orderID=" + URLEncoder.encode(String.valueOf(order.getOrderId()), "UTF-8") +
                    "&orderTotal=" + URLEncoder.encode(String.valueOf(order.getOrderTotal()), "UTF-8") +
                    "&orderNumber=" + URLEncoder.encode(String.valueOf(order.getOrderNumber()), "UTF-8") +
                    "&orderDate=" + URLEncoder.encode(order.getOrderDate().toString(), "UTF-8") +
                    "&drinkName=" + URLEncoder.encode(currentDrink.getDrinkName(), "UTF-8") +
                    "&drinkQty=" + URLEncoder.encode(String.valueOf(currentDrink.getDrinkQty()), "UTF-8");

            connection = (HttpURLConnection) url.openConnection();
            connection.setDoOutput(true);
            connection.setRequestMethod("POST");

            connection.setFixedLengthStreamingMode(postContent.getBytes().length);
            connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            PrintWriter outStream = new PrintWriter(connection.getOutputStream());
            outStream.print(postContent);
            outStream.close();

            String response = "";
            Scanner inStream = new Scanner(connection.getInputStream());

            while (inStream.hasNextLine()) {
                response += (inStream.nextLine());
            }
            System.out.println("PHP Response: " + response);

        }
    }

    public String getOrderProgress(int orderID, int orderNumber) throws Exception{
        URL url = new URL("http://134.114.8.88/website/getOrderProgress.php");
        HttpURLConnection connection;

        String postContent = "&orderID=" + URLEncoder.encode(String.valueOf(orderID), "UTF-8") +
                             "&orderNumber=" + URLEncoder.encode(String.valueOf(orderNumber), "UTF-8");

        connection = (HttpURLConnection) url.openConnection();
        connection.setDoOutput(true);
        connection.setRequestMethod("POST");

        connection.setFixedLengthStreamingMode(postContent.getBytes().length);
        connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
        PrintWriter outStream = new PrintWriter(connection.getOutputStream());
        outStream.print(postContent);
        outStream.close();

        String response = "";
        Scanner inStream = new Scanner(connection.getInputStream());

        while (inStream.hasNextLine()) {
            response += (inStream.nextLine());
        }
        System.out.println("PHP Response to Order Progress: " + response);

        return response;
    }

    public void updateOrderTotal(int orderID, double orderTotal, int orderNumber) throws Exception{
        URL url = new URL("http://134.114.8.88/website/updateOrderTotal.php");
        HttpURLConnection connection;

        String postContent = "&orderID=" + URLEncoder.encode(String.valueOf(orderID), "UTF-8") +
                             "&orderTotal=" + URLEncoder.encode(String.valueOf(orderTotal), "UTF-8") +
                             "&orderNumber=" + URLEncoder.encode(String.valueOf(orderNumber));

        connection = (HttpURLConnection) url.openConnection();
        connection.setDoOutput(true);
        connection.setRequestMethod("POST");

        connection.setFixedLengthStreamingMode(postContent.getBytes().length);
        connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
        PrintWriter outStream = new PrintWriter(connection.getOutputStream());
        outStream.print(postContent);
        outStream.close();

        String response = "";
        Scanner inStream = new Scanner(connection.getInputStream());

        while (inStream.hasNextLine()) {
            response += (inStream.nextLine());
        }
        System.out.println("UPDATED?: " + response);

    }
}




