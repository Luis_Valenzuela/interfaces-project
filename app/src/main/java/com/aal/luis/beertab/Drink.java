package com.aal.luis.beertab;

/**
 * Created by Luis Valenzuela on 3/17/2016.
 */

/*
* This class stores all of the information related to a drink.
* */
public class Drink {

    private int drinkID = 0;
    private String drinkName = null;
    private Double drinkCost = .0;
    private int drinkImageId = 0;
    private int drinkQty = 0;
    private String drinkDescription = null;
    private String drinkCat = null;

    public Drink(){}

    public int getDrinkID() {
        return drinkID;
    }

    public void setDrinkID(int inputDrinkID) {
        drinkID = inputDrinkID;
    }

    public void setDrinkdescription(String description) {
        drinkDescription = description;
    }

    public void setDrinkCat(String cat) {
        drinkCat = cat;
    }
    public String getDrinkName() {
        return drinkName;
    }

    public String getDrinkDescription() {
        return drinkDescription;
    }

    public String getDrinkCat() {
        return drinkCat;
    }

    public void setDrinkName(String inputDrinkName) {
        drinkName = inputDrinkName;
    }

    public double getDrinkCost() {
        return drinkCost;
    }

    public void setDrinkCost(double inputDrinkCost) {
        drinkCost = inputDrinkCost;
    }

    public Integer getDrinkImageId() {
        return drinkImageId;
    }

    public void setDrinkImageId(int inputDrinkImageId) {
        drinkImageId = inputDrinkImageId;
    }

    public int getDrinkQty() {
        return drinkQty;
    }

    public void setDrinkQty(int inputDrinkQty) {
        drinkQty = inputDrinkQty ;
    }


}
