package com.aal.luis.beertab;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.view.ContextThemeWrapper;

/**
 * Created by Luis Valenzuela on 3/17/2016.
 */
public class DatabaseHelper extends SQLiteOpenHelper{
    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "beertab.db";
    public static final String TABLE_DRINK = "drink";

    public static final String COLUMN_ID = "_id";
    public static final String ORDER_ID = "orderID";
    public static final String COLUMN_DRINK_NAME = "drinkName";
    public static final String COLUMN_COST = "drinkPrice";
    public static final String COLUMN_IMAGE_ID = "drinkImageID";
    private static final String CREATE_TABLE_USERS = "CREATE TABLE users(_id INTEGER PRIMARY KEY, username TEXT, password TEXT)";


    public DatabaseHelper(Context context){
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase DB) {
        String CREATE_DRINK_TABLE = "CREATE TABLE " + TABLE_DRINK + "(" + COLUMN_ID + "INTEGER PRIMARY KEY" + ")";
        DB.execSQL(CREATE_DRINK_TABLE);
        DB.execSQL(CREATE_TABLE_USERS);
    }

    @Override
    public void onUpgrade(SQLiteDatabase DB, int oldVersion, int newVersion) {
        DB.execSQL("DROP TABLE IF EXISTS " + TABLE_DRINK);
        onCreate(DB);
        DB.execSQL("DROP TABLE IF EXISTS users");
    }

    public void addDrink(Drink drink) {
        ContentValues values = new ContentValues();

        values.put(COLUMN_DRINK_NAME, drink.getDrinkName());
        values.put(COLUMN_COST, drink.getDrinkCost());
        values.put(COLUMN_IMAGE_ID, drink.getDrinkImageId());

        SQLiteDatabase DB = this.getWritableDatabase();
        DB.insert(TABLE_DRINK, null, values);
    }

    public Drink getDrink (String drinkName) {
        String query = "SELECT * FROM " + TABLE_DRINK + " WHERE " + COLUMN_DRINK_NAME + " = \"" + drinkName + "\"";

        SQLiteDatabase DB = this.getWritableDatabase();

        Cursor cursor = DB.rawQuery(query, null);

        Drink drink = new Drink();

        if (cursor.moveToFirst()) {
            cursor.moveToFirst();
            drink.setDrinkID(Integer.parseInt(cursor.getString(0)));
            drink.setDrinkName(cursor.getString(1));
            drink.setDrinkCost(cursor.getFloat(2));
            //drink.setDrinkImageId(cursor.getString(3));
            cursor.close();
        }
        else {
            drink = null;
        }
        DB.close();
        return drink;
    }

    public boolean deleteDrink(String drinkName) {
        boolean result = false;

        String query = "SELECT * FROM " + TABLE_DRINK + " WHERE " + COLUMN_DRINK_NAME + " = \"" + drinkName + "\"";

        SQLiteDatabase DB = this.getWritableDatabase();

        Cursor cursor = DB.rawQuery(query, null);

        Drink drink = new Drink();

        if (cursor.moveToFirst()) {
            drink.setDrinkID(Integer.parseInt(cursor.getString(0)));
            DB.delete(TABLE_DRINK, COLUMN_ID + " =?", new String[] {String.valueOf(drink.getDrinkID())});
            cursor.close();
            result = true;
        }
        DB.close();
        return result;
    }



    public boolean userLogin(String username, String password){
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.query("users", new String[] {"_id"}, "username=? and password=?", new String[]{username, password}, null, null,null);
        if (cursor.getCount() != 0){
            db.close();
            return true;
        }
        else{
            db.close();
            return false;
        }

    }

    public String newUser(String username, String password){
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.query("users", new String[]{"_id"}, "username=?", new String[]{username}, null, null, null, null);
        if(cursor.getCount() != 0){
            db.close();
            return "That username is taken.";
        }
        else {
            db = getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put("username", username);
            values.put("password", password);
            db.insert("users", null, values);
            db.close();
            return "User created!";
        }
    }

    public String getUserID(String username){
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.query("users", new String[]{"_id"}, "username=?", new String[]{username}, null, null, null, null);
        cursor.moveToFirst();
        return Integer.toString(cursor.getInt(0));
    }
}
